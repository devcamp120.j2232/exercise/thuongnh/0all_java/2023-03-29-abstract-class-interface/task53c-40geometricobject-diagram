package models;


// đối tượng hình chữ nhật

public class Retangle implements GeometricObject {

    private double whith;
    private double length;
    //  hàm khởi tạo 
    public Retangle(double whith, double length) {
        this.whith = whith;
        this.length = length;
    
    }
  

    // in ra màn hình 
    @Override
    public String toString() {
        return "Retangle [whith=" + whith + ", length=" + length + "]";
    }
    
      // diện tích 
      public double getArea(){
        return whith * length;
    }
    // chu vi
    public double getPerimeter(){
        return 2 * whith * length;
    }
    
    
}
