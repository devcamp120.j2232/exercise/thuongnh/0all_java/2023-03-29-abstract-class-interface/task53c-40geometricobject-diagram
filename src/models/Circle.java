package models;

public class Circle implements GeometricObject {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }
    // diện tích 
    public double getArea(){
        return Math.PI + Math.pow(radius, 2);
    }
    // chu vi
    public double getPerimeter(){
        return 2*  Math.PI + radius;
    }

    
    
    
}
