package models;

public interface  GeometricObject {
    public double getArea();
    public double getPerimeter();
    
}
