import models.Circle;
import models.Retangle;

public class App {
    public static void main(String[] args) throws Exception {

        // tạo đối tượng hình tròn 
        Circle circle = new Circle(1);
        // tạo đối tượng hình chữ nhật
        Retangle retangle = new Retangle(10,   15);

         // chu vi diện tích hình tròn
        System.out.println(circle);
        System.out.println(circle.getArea());
        System.out.println(circle.getPerimeter());



        // chu vi diện tích hình chữ nhật 
        System.out.println(retangle);
        System.out.println(retangle.getArea());
        System.out.println(retangle.getPerimeter());


       

    }
}
